###### Dependencies ######

import networkx as nx
from networkx.algorithms.community.centrality import girvan_newman
from networkx import edge_betweenness_centrality as betweenness

###### Definitions ######

# This is multiplier that can be used to increase the weight of the edges in the
# metagraph. These weights are floating-point numbers. You can configure this
# multiplier based on the needs of your analysis.
WEIGHT_MULTIPLIER = 1

###### Functions ######

def CreateGraphFromGroups(groups):
    assert isinstance(groups, dict)

    G = nx.DiGraph()

    for identifier in groups:
        websitesList = groups[identifier]
        assert isinstance(websitesList, list)

        for website in websitesList:
            G.add_edge(website, identifier)

    print("   Constructed bipartite graph with", len(G.nodes), "nodes and", len(G.edges), "edges")

    return G

def CombineDiGraphs(graph1, graph2):
    assert isinstance(graph1, nx.DiGraph)
    assert isinstance(graph2, nx.DiGraph)

    combined = nx.DiGraph()

    for edge in graph1.edges():
        combined.add_edge(*edge)

    for edge in graph2.edges():
        combined.add_edge(*edge)

    return combined

def DetermineGroupsWeights(publisherIDsGroup, containerIDsGroup, trackingIDsGroup, measurementIDsGroup):
    assert isinstance(publisherIDsGroup, dict)
    assert isinstance(containerIDsGroup, dict)
    assert isinstance(trackingIDsGroup, dict)
    assert isinstance(measurementIDsGroup, dict)

    pubEdgeWeight = 1 / len(publisherIDsGroup) * WEIGHT_MULTIPLIER
    print("   There are", len(publisherIDsGroup), "publisher ID groups. Each publisherID edge weighs %.6f" % pubEdgeWeight)

    contEdgeWeight = 1 / len(containerIDsGroup) * WEIGHT_MULTIPLIER
    print("   There are", len(containerIDsGroup), "container ID groups. Each containerID edge weighs %.6f" % contEdgeWeight)

    combinedWeight = 1 / ( len(trackingIDsGroup) + len(measurementIDsGroup) ) * WEIGHT_MULTIPLIER
    print("   There are", len(trackingIDsGroup), "tracking ID groups.")
    print("   There are", len(measurementIDsGroup), "measurement ID groups.")
    print("   Each analyticsID edge weighs %.6f" % combinedWeight)

    return (pubEdgeWeight, contEdgeWeight, combinedWeight)

def CreateMetagraph():
    return nx.Graph()

def CreateMetagraphEdges(graph, group, weight):
    assert isinstance(graph, nx.Graph)
    assert isinstance(group, dict)
    assert isinstance(weight, int) or isinstance(weight, float)

    for identifier in group:
        assert isinstance(identifier, str)

        websites = group[identifier]

        for i in range(0, len(websites)):
            for j in range(i + 1, len(websites)):
                start = websites[i]
                end = websites[j]

                graph.add_edge(start, end)

                edgeData = graph.get_edge_data(start, end)

                if "weight" not in edgeData:
                    edgeData["weight"] = weight
                else:
                    edgeData["weight"] += weight

def GetGraphWithEdgesMoreThan(graph, threshold):
    assert isinstance(graph, nx.Graph)
    assert isinstance(threshold, float)

    result = nx.Graph()

    for edge in graph.edges:
        edgeData = graph.get_edge_data(*edge)
        assert isinstance(edgeData, dict)
        assert "weight" in edgeData

        weight = edgeData["weight"]

        if (weight >= threshold):
            result.add_edge(*edge)
            edgeData = result.get_edge_data(*edge)
            edgeData["weight"] = weight

    return result

def GirvanNewmanCommunity(graph):
    assert isinstance(graph, nx.Graph)

    communities = girvan_newman(graph)

    groups = []
    for group in next(communities):
        group = list(group)
        assert isinstance(group, list)
        group.sort()
        groups.append(list(group))

    return groups
