###### Dependencies ######

import storage as Storage
import graphs as Graphs
from identifiers import GroupOnIdentifiers, FilterGroups

###### Definitions ######

# This is the input file that contains the data from the crawling process.
# This file should be a JSON array of objects that contain the following fields:
#
# website: The domain from the Tranco list
# landingURL: The actual URL of the landing page
# publisherIDs: An array with the publisher IDs that the website contains
# trackingIDs: An array with the tracking IDs that the website contains
# measurementIDs: An array with the measurement IDs that the website contains
# containerIDs: An array with the container IDs that the website contains
INPUT_FILE = "./crawl_data.json"

# This is the path where the graphs and communities will be stored
OUTPUT_DIR = "./graphs/"

# The threshold of weight that an edge should have in order to remain in the
# metagraph. This is related to the edge pruning operation. Choose this value
# carefully. It depends on the WEIGHT_MULTIPLIER value, as well as the ratio of
# edges that you want to keep in the metagraph.
WEIGHT_THRESHOLD = 0.0002

###### Main ######

def main():
    print(">> Application started")
    Storage.EnsureDirecroryExists(OUTPUT_DIR)

    # Load data
    data = Storage.LoadJsonData(INPUT_FILE)
    print(">> Loaded crawled data")

    # Group websites based on the identifiers they contain. There is a group for
    # each unique identifier. Websites that contain the same identifier will be
    # placed in the same group. A website can be found in more than one groups.
    (publisherIDsGroup, trackingIDsGroup, measurementIDsGroup, containerIDsGroup) = GroupOnIdentifiers(data)
    print(">> Grouped websites based on the identifiers they contain")

    # Create the bipartite graphs based on the grouping process. Specifically, when
    # a websites X contains an identifier Y, there will be an edge X->Y in the
    # respective bipartite graph.
    bipartite_publisherIDs   = Graphs.CreateGraphFromGroups(publisherIDsGroup)
    bipartite_trackingIDs    = Graphs.CreateGraphFromGroups(trackingIDsGroup)
    bipartite_measurementIDs = Graphs.CreateGraphFromGroups(measurementIDsGroup)
    bipartite_containerIDs   = Graphs.CreateGraphFromGroups(containerIDsGroup)
    print(">> Constructed bipartite graphs")

    # Measurement IDs and Container IDs are placed into the same graph since
    # they represent the same service and have similar functionality.
    bipartite_analyticsIDs = Graphs.CombineDiGraphs(bipartite_trackingIDs, bipartite_measurementIDs)
    print(">> Combined measurement and tracking IDs graphs into a single bipartite graph")

    Storage.StoreGraph(bipartite_publisherIDs, OUTPUT_DIR + "bipartite_publisherIDs.gpickle")
    Storage.StoreGraph(bipartite_analyticsIDs, OUTPUT_DIR + "bipartite_analyticsIDs.gpickle")
    Storage.StoreGraph(bipartite_containerIDs, OUTPUT_DIR + "bipartite_containerIDs.gpickle")
    print(">> Stored bipartite graphs")

    # Exclude identifiers which can be found in only one website. These
    # identifiers have no value in the metagraph. They do not provide any
    # information about relationships among websites.
    publisherIDsGroup = FilterGroups(publisherIDsGroup)
    trackingIDsGroup = FilterGroups(trackingIDsGroup)
    measurementIDsGroup = FilterGroups(measurementIDsGroup)
    containerIDsGroup = FilterGroups(containerIDsGroup)
    print(">> Removed unimportant identifiers")

    # Determine how much each common identifier increases the weight of a meta-edge.
    # Each shared identifier increases the weight of the meta-edge by 1/n, where n
    # is the total number of distinct identifiers of this type found in more than
    # one website.
    (pubEdgeWeight, contEdgeWeight, analyticsWeight) = Graphs.DetermineGroupsWeights(publisherIDsGroup, containerIDsGroup, trackingIDsGroup, measurementIDsGroup)
    print(">> Determined edge weights for metagraph")

    # Create and store the metagraph
    metagraph = Graphs.CreateMetagraph()
    Graphs.CreateMetagraphEdges(metagraph, publisherIDsGroup, pubEdgeWeight)
    Graphs.CreateMetagraphEdges(metagraph, containerIDsGroup, contEdgeWeight)
    Graphs.CreateMetagraphEdges(metagraph, trackingIDsGroup, analyticsWeight)
    Graphs.CreateMetagraphEdges(metagraph, measurementIDsGroup, analyticsWeight)
    print(">> Created metagraph with", len(metagraph.nodes), "nodes and", len(metagraph.edges), "edges")

    Storage.StoreGraph(metagraph, OUTPUT_DIR + "metagraph.gpickle")
    print(">> Stored full-size metagraph")

    # Perform edge-pruning. This is done to remove noise from the metagraph
    metagraph = Graphs.GetGraphWithEdgesMoreThan(metagraph, WEIGHT_THRESHOLD)
    print(">> Performed edge pruning")
    print("   Metagraph now contains", len(metagraph.nodes), "nodes and", len(metagraph.edges), "edges")

    # Execute a community detection algorithm. This will return sets of websites
    # which are operated by the same legal entity.
    communities = Graphs.GirvanNewmanCommunity(metagraph)
    print(">> Found", len(communities), "communities of websites")

    Storage.StoreJson(communities, OUTPUT_DIR + "communities.json")
    print(">> Stored communities")

if __name__ == "__main__":
    main()