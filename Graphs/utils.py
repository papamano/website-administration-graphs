###### Functions ######

def AddToSubList(originalList, key, value):
    assert isinstance(originalList, dict)
    assert isinstance(key, str)

    if (key not in originalList):
        originalList[key] = []

    if (value not in originalList[key]):
        originalList[key].append(value)
