###### Dependencies ######

import re

from urllib.parse import urlsplit
from utils import AddToSubList

###### Definitions ######

# Simple flag to indicate whether some identifiers should be excluded from the
# metagraph. This is optional.
EXCLUDE_IDENTIFIERS = False

# The list of identifiers to exclude from the metagraph
EXCLUDED_IDENTIFIERS = [ ]

###### Functions ######

def GroupOnIdentifier(data, identifier):
    assert isinstance(data, list)
    assert isinstance(identifier, str)

    groups = {}

    for entry in data:
        assert "landingURL" in entry
        assert identifier in entry

        website = entry["landingURL"]
        assert isinstance(website, str)

        if (website.startswith("about:blank")):
            continue

        url = "{0.netloc}".format(urlsplit(website))
        assert isinstance(url, str) and len(url) > 0

        identifiers = entry[identifier]
        assert isinstance(identifiers, list)

        for key in identifiers:
            if (re.search("^UA", key) and re.search("-\d+$", key)):
                key = key[0: key.rfind("-")]
            AddToSubList(groups, key, url)

    print("   Found", len(groups), "unique", identifier)
    return groups

def GroupOnIdentifiers(data):
    assert isinstance(data, list)

    publisherIDsGroup = GroupOnIdentifier(data, "publisherIDs")
    trackingIDsGroup = GroupOnIdentifier(data, "trackingIDs")
    measurementIDsGroup = GroupOnIdentifier(data, "measurementIDs")
    containerIDsGroup = GroupOnIdentifier(data, "containerIDs")

    return (publisherIDsGroup, trackingIDsGroup, measurementIDsGroup, containerIDsGroup)

def FilterGroups(groups):
    assert isinstance(groups, dict)

    result = {}

    for key in groups:
        if (key in EXCLUDED_IDENTIFIERS) and EXCLUDE_IDENTIFIERS:
            continue

        websitesList = groups[key]
        assert isinstance(websitesList, list)

        if len(websitesList) > 1:
            result[key] = websitesList

    return result