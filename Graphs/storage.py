###### Dependencies ######

import json
import networkx as nx
from pathlib import Path

###### Functions ######

def LoadJsonData(path):
    assert isinstance(path, str)

    with open(path) as file:
        data = json.load(file)

    return data

def StoreJson(data, filename):
    assert isinstance(data, list) or isinstance(data, dict)
    assert isinstance(filename, str)

    with open(filename, "w") as file:
        file.write(json.dumps(data, indent = 2))

def StoreGraph(graph, path):
    assert isinstance(graph, nx.Graph)
    assert isinstance(path, str)

    nx.write_gpickle(graph, path)

def EnsureDirecroryExists(path):
    assert isinstance(path, str)
    Path(path).mkdir(parents = True, exist_ok = True)