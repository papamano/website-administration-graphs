# Acknowledgments

This project received funding from the **EU H2020 Research and Innovation** programme under grant agreements No 830927 (Concordia), No 830929 (CyberSec4Europe), No 871370 (Pimcity) and No 871793 (Accordion). These results reflect only the authors’ view and the Commission is not responsible for any use that may be made of the information it contains.

## Resources

Project's icon made by [Becris](https://www.flaticon.com/authors/becris) from [Flaticon](https://www.flaticon.com/).