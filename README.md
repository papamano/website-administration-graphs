# Website Administration Graphs

## About

A variation of this tool was used in the following project. If you use this tool or its source code in any way, please cite our work.

Emmanouil Papadogiannakis, Panagiotis Papadopoulos, Nicolas Kourtellis, and Evangelos P Markatos, "[**Leveraging Google's Publisher-Specific IDs to Detect Website Administration**](https://arxiv.org/abs/2202.05074)"

The paper can be found:
* DOI: [10.1145/3485447.3512124](https://doi.org/10.1145/3485447.3512124)
* Open-access version in [arXiv](https://arxiv.org/abs/2202.05074)

## Introduction

Digital advertising is the most popular way for content monetization on the Internet. Publishers spawn new websites, and older ones change hands with the sole purpose of monetizing user traffic. In this ever-evolving ecosystem, it is challenging to effectively tell: Which entities monetize what websites? What categories of websites does an average entity typically monetize on and how diverse are these websites? How has this website administration ecosystem changed across time?

In this paper, we propose a novel, **graph-based** methodology to detect administration of sites on the Web by exploiting the ad-related **publisher-specific IDs**. We apply our methodology across the top 1M websites and study the characteristics of the created graphs of website administration. Our findings show that approximately 90% of the websites are associated with a single publisher and that small publishers tend to manage less popular websites. We perform a historical analysis of up to 8M websites, and we find a new constantly arising number of (intermediary) publishers that control and monetize traffic from hundreds of websites, seeking a share of the ad-market pie. We see that over the years, websites tend to move from big to smaller administrators.

## Description

This repository contains a simple tool that can detect communities of websites operated by the same entity. The user only needs to provide a JSON file with the identifiers that each website contains. The tool will create both the bipartite graphs and the **metagraph** and produce the communities of websites, as explained in the paper.

Additionally, in [**Data/ownership.json**](Data/ownership.json) we provide a list of 112 distinct communities of varios sizes, consisting of over 1,280 websites.
These communities have been manually evaluated and each community consists of websites **owned** by the same legal entity.

## Notes

* The source code for visiting websites and collecting data can be found [**here**](https://gitlab.com/papamano/scrape-titan).

* For more instructions on how to use the tool please read the comments in the respective files.

* This project is publicly available in order to contribute to the scientific community and to support further research on website administration and ownership.

## Prerequisites

Required Software:
* python3 (tested with version *v3.8.10*)

Packages:
* [Python Standard Library](https://docs.python.org/3/library/)
* [NetworkX](https://networkx.org/)

## Execution

To download the project:
```bash
git clone https://gitlab.com/papamano/website-administration-graphs.git
```

To generate the graphs and extract communities:
```bash
cd website-administration-graphs/Graphs/
python3 app.py
```
## Contributors

* [Papadogiannakis Manos](https://gitlab.com/papamano/)

## Support

Please contact one of the project's contributors.

## License

This project is released under under the Mozilla Public License Version 2.0

Make sure you read the provided End-User License Agreement (EULA).
By installing, copying or otherwise using this Software, you agree to be bound
by the terms of the provided End EULA
